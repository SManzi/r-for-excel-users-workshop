R for Excel Users Workshop
==========================

## Introduction to the R for Excel Users Workshop
This workshop was developed for the NHS-R conference 2019 in Birmingham UK. The aim of the workshop was to teach people how to develop generic basic R scripts to automate routine health service analysis tasks. During the workshop this was achieved by:  
* Learning to identify which tasks are most appropriate for automation
* Understanding how to break down an analysis into useful stages
* Undertaking a practical task to reconstruct a non-generic R script of an example analysis to learn about the basic construction of R scripts and the ordering of operations
* Undertaking a second practical task to reconstruct a more generic version of the R script used in the first task to learn about the evolution of script from a non-generic to a generic script  

[Follow this link to the git repository containing all of the workshop files](https://gitlab.com/SManzi/r-for-excel-users-workshop)

The files contained in the repository are:
* The workshop presentation without the task answers
* The workshop presentation with the task answers
* The mixed up code for task 1
* The correctly ordered code for task 1
* The mixed up code for task 2
* The correctly ordered code for task 2
* The raw data used for tasks 1 and 2
* The R script used to create the raw data for the workshop
* A list of required R packages
